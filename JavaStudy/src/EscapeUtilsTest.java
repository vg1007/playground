import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Test;

public class EscapeUtilsTest {
    @Test
    public void test1() {
        System.out.println(StringEscapeUtils.escapeJava("ab''c"));
        System.out.println(StringEscapeUtils.escapeJava ("ab''c"));
        System.out.println(StringEscapeUtils.escapeJava("ab\"c"));
        System.out.println("ab\"c");
        System.out.println(StringEscapeUtils.escapeJava("ab\tc"));
        System.out.println(StringEscapeUtils.escapeJava("ab\tc"));
        System.out.println(StringEscapeUtils.escapeJava("ab\\\"c"));
    }
}

