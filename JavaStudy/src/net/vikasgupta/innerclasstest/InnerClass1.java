//package net.vikasgupta.innerclasstest;
//
//public class InnerClass1 {
//	public class Inner1 {}
//	protected class Inner2 {}
//	private class Inner3 {}
//	class Inner4{}
//}
//
//class InnerClass1Test {
//
//	public static void main(String[] args) {
//		InnerClass1.Inner1 ici1;
//		InnerClass1.Inner2 ici2;
//		InnerClass1.Inner3 ici3; 	//Inner3 is not visible as it is private
//		InnerClass1.Inner4 ici4;
//	}
//}
